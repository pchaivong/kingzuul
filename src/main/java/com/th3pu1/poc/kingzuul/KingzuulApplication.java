package com.th3pu1.poc.kingzuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


@EnableZuulProxy
@SpringBootApplication
public class KingzuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(KingzuulApplication.class, args);
	}
}
