package com.th3pu1.poc.kingzuul;

import com.netflix.client.config.IClientConfig;
import com.netflix.client.config.IClientConfigKey;
import com.netflix.loadbalancer.AvailabilityFilteringRule;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;
import io.fabric8.spring.cloud.kubernetes.ribbon.KubernetesServerList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pchaivong on 12/4/2017 AD.
 */

@Configuration
public class RibbonConfiguration {

    @Autowired
    private IClientConfig clientConfig;

    @Bean
    public IPing ribbonPing(IClientConfig clientConfig){
        return new PingUrl();
    }

    @Bean
    public IRule ribbonRule(IClientConfig config){
        return new AvailabilityFilteringRule();
    }

    @Bean
    public IClientConfig clientConfig(){
        return IClientConfig.Builder.newBuilder()
                .withDefaultValues()
                .build()
                .set(IClientConfigKey.Keys.NIWSServerListClassName, KubernetesServerList.class.getName());
    }
}
